'use strict';

/* Services */
//////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////W E A T H E R      S E R V I C E S /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

var categoryServices = angular.module('AutoShopServices', []);
categoryServices.factory('AutoShopService', function($http) {
    return {
        getCustomers: function(){
            return $http.get('./server/webservices/rest.php/customers');
        },
        getCustomerInformationByCustomerId: function(customerId){
            return $http.get('./server/webservices/rest.php/customer/information/' + customerId);
        },
        getSalesmen: function(){
            return $http.get('./server/webservices/rest.php/salesmen');
        },
        getSales: function(){
            return $http.get('./server/webservices/rest.php/sales');
        },
        getSale: function(orderId, orderType){
            return $http.get('./server/webservices/rest.php/sale/' + orderId + '/' + orderType);
        },
        getMechanics: function(){
            return $http.get('./server/webservices/rest.php/mechanics' );
        },
        getWorkOrdersForMechanic: function(employeeId){
            return $http.get('./server/webservices/rest.php/mechanic/workorders/' + employeeId);
        },
        getWorkOrderByOrderId: function(orderId){
            return $http.get('./server/webservices/rest.php/workorder/' + orderId);
        },
        getSalesMansSalesOrders: function(employeeId){
            return $http.get('./server/webservices/rest.php/salesman/salesorders/' + employeeId);
        },
        getSalesOrderByOrderId: function(orderId){
            return $http.get('./server/webservices/rest.php/salesorder/' + orderId);
        }
    }
});
