'use strict';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////M O D U L E   A P P    R E G I S T R A T I O N/////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var customerApp = angular.module('customerApp', [
    'ngRoute',
    'AutoShopHomeController',
    'SalesmenController',
    'MechanicsController',
    'MechanicsWorkOrdersController',
    'SalesmanSalesOrdersController',
    'SalesController',
    'SaleController',

    'AutoShopServices',

    'AutoShopDirectives',

    'ngTouch'

]);

customerApp.filter("sanitize", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////          M O D U L E   R O U T E R           /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
customerApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode(false);
        $routeProvider.
            when('/autoshop/home', {
                templateUrl: 'partials/controllertemplates/autoshophomecontrollerview.html',
                controller: 'AutoShopHomeController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).
            when('/autoshop/salesmen', {
                templateUrl: 'partials/controllertemplates/salesmancontrollerview.html',
                controller: 'SalesmenController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).
            when('/autoshop/salesman/salesorders', {
                templateUrl: 'partials/controllertemplates/salesmansalesorderscontrollerview.html',
                controller: 'SalesmanSalesOrdersController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).
            when('/autoshop/mechanics', {
                templateUrl: 'partials/controllertemplates/mechanicscontrollerview.html',
                controller: 'MechanicsController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).
           when('/autoshop/mechanic/workorders', {
               templateUrl: 'partials/controllertemplates/mechanicsworkorderscontrollerview.html',
               controller: 'MechanicsWorkOrdersController',
               access: {
                   name: "login",
                   isFree: true
               }

           }).
            when('/autoshop/sales', {
            templateUrl: 'partials/controllertemplates/salescontrollerview.html',
            controller: 'SalesController',
            access: {
               name: "login",
               isFree: true
            }
            }).
            when('/autoshop/sale', {
                templateUrl: 'partials/controllertemplates/salecontrollerview.html',
                controller: 'SaleController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).

            otherwise({
                redirectTo: '/autoshop/home/'
            });

    }]
);





