'use strict';


var autoShopHomeController = angular.module('AutoShopHomeController', ['ui.bootstrap']);
autoShopHomeController.controller('AutoShopHomeController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);

var salesmenController = angular.module('SalesmenController', ['ui.bootstrap']);
salesmenController.controller('SalesmenController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);


var salesmanSalesOrdersController = angular.module('SalesmanSalesOrdersController', ['ui.bootstrap']);
salesmanSalesOrdersController.controller('SalesmanSalesOrdersController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);

var mechanicsController = angular.module('MechanicsController', ['ui.bootstrap']);
mechanicsController.controller('MechanicsController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);

var mechanicsWorkOrdersController = angular.module('MechanicsWorkOrdersController', ['ui.bootstrap']);
mechanicsWorkOrdersController.controller('MechanicsWorkOrdersController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);

var salesController = angular.module('SalesController', ['ui.bootstrap']);
salesController.controller('SalesController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);

var saleController = angular.module('SaleController', ['ui.bootstrap']);
saleController.controller('SaleController', ['$scope', '$routeParams' , '$location', 'AutoShopService',
    function($scope, $routeParams, $location, AutoShopService) { }
]);

