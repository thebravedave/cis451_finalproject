'use strict';

angular.module('AutoShopDirectives', ['ui.bootstrap'])



.directive('autoShopMenu', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location){
            init();
            function init(){}


        },
        templateUrl:"./partials/directivetemplates/autoshopmenudirectiveview.html"
    }
})

.directive('autoShopHome', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, AutoShopService){
            init();
            function init(){
                $scope.customerId = null;
                getCustomers();
                initializeEventListeners();
            }
            function getCustomers(){
                AutoShopService.getCustomers().success(function(data){
                    if(data.success == true){

                        $scope.customers = data.message;

                    }

                }).error(function(){
                    //todo then there was an error
                });
            }
            function initializeEventListeners(){
                $scope.$watch('customerId', function(data){

                    if($scope.customerId != null){
                        AutoShopService.getCustomerInformationByCustomerId($scope.customerId).success(function(data){

                            $scope.customersOrders = data.message;
                        }).error(function(){

                        });
                    }
                });



            }

        },
        templateUrl:"./partials/directivetemplates/autoshophomedirectiveview.html"
    }
})



.directive('mechanics', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, $routeParams, AutoShopService){
            init();
            function init(){
                $scope.mechanics = null;
                getMechanics();
                initializeEventListeners();
            }
            function getMechanics(){
                AutoShopService.getMechanics().success(function(data){
                    if(data.success == true){

                        $scope.mechanics = data.message;

                    }

                }).error(function(){});
            }
            function initializeEventListeners(){}

            $scope.loadWorkOrders = function(employeeId){


                $location.search({});

                $location.search('employeeId', employeeId);
                var path = '/autoshop/mechanic/workorders';
                $location.path(path);
            }

        },
        templateUrl:"./partials/directivetemplates/mechanicsdirectiveview.html"
    }
})


.directive('mechanicsWorkOrders', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, AutoShopService){
            init();
            function init(){

                $scope.employeeId = $location.search().employeeId;
                if($scope.employeeId == null || $scope.employeeId == undefined)
                {
                    //then they came to this page with no employee id so send them back to the home page
                    var path = '/autoshop/home';
                    $location.path(path);
                }

                $scope.mechanistWorkOrders = null;
                getMechanicsWorkOrders();
                initializeEventListeners();

            }
            function getMechanicsWorkOrders(){
                AutoShopService.getWorkOrdersForMechanic($scope.employeeId).success(function(data){

                    if(data.success == true){
                        $scope.mechanistWorkOrders = data.message;

                    }

                }).error(function(){
                    //todo then there was an error
                });
            }
            function initializeEventListeners(){}

            $scope.loadSales = function(employeeId){
                alert(employeeId);
            }

        },
        templateUrl:"./partials/directivetemplates/mechanicsworkordersdirectiveview.html"
    }
})

.directive('salesmen', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, $routeParams, AutoShopService){
            init();
            function init(){
                $scope.salesmen = null;
                getSalesmen();
                initializeEventListeners();
            }
            function getSalesmen(){
                AutoShopService.getSalesmen().success(function(data){
                    if(data.success == true){

                        $scope.salesmen = data.message;

                    }

                }).error(function(){
                    //todo then there was an error
                });
            }
            function initializeEventListeners(){}

            $scope.loadSales = function(employeeId){


                $location.search({});

                $location.search('employeeId', employeeId);
                var path = '/autoshop/salesman/salesorders';
                $location.path(path);
            }

        },
        templateUrl:"./partials/directivetemplates/salesmendirectiveview.html"
    }
})

.directive('salesmanSalesOrders', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, AutoShopService){
            init();
            function init(){


                $scope.employeeId = $location.search().employeeId;
                if($scope.employeeId == null || $scope.employeeId == undefined)
                {
                    //then they came to this page with no employee id so send them back to the home page
                    var path = '/autoshop/home';
                    $location.path(path);
                }

                $scope.salesmen = null;
                getSalesmanSalesOrders();
                initializeEventListeners();

            }
            function getSalesmanSalesOrders(){
                AutoShopService.getSalesMansSalesOrders($scope.employeeId).success(function(data){
                    if(data.success == true){
                        $scope.salesmanSalesOrders = data.message;

                    }

                }).error(function(){
                    //todo then there was an error
                });
            }
            function initializeEventListeners(){}

            $scope.loadSales = function(employeeId){
                alert(employeeId);
            }

        },
        templateUrl:"./partials/directivetemplates/salesmansalesordersdirectiveview.html"
    }
})


.directive('sales', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, $routeParams, AutoShopService){
            init();
            function init(){
                $scope.salesmen = null;
                getSalesmen();
                initializeEventListeners();
            }
            function getSalesmen(){
                AutoShopService.getSales().success(function(data){
                    if(data.success == true){
                        $scope.sales = data.message;
                    }

                }).error(function(){});
            }
            function initializeEventListeners(){}

            $scope.loadSale = function(orderId, orderType){
                $location.search({});

                $location.search('orderId', orderId);
                $location.search('orderType', orderType);
                var path = '/autoshop/sale';
                $location.path(path);
            }

        },
        templateUrl:"./partials/directivetemplates/salesdirectiveview.html"
    }
})

.directive('sale', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, $routeParams, AutoShopService){
            init();
            function init(){
                $scope.salesmanSalesOrders = null;
                $scope.mechanistWorkOrders = null;
                var orderId = $location.search().orderId;
                var orderType = $location.search().orderType;
                if(orderId == null || orderId == undefined || orderType == null || orderType == undefined){
                    var path = '/autoshop/home';
                    $location.path(path);
                }
                $scope.orderType = orderType;
                $scope.orderId = orderId;
                loadSale(orderId, orderType);
            }
            function loadSale(orderId, orderType){
                AutoShopService.getSale(orderId, orderType).success(function(data){
                    if(data.success == true){
                        if(orderType == "sales order"){
                            $scope.salesmanSalesOrders = data.message;
                        }
                        else{
                            $scope.mechanistWorkOrders = data.message;
                        }

                    }

                }).error(function(){});
            }

        },
        templateUrl:"./partials/directivetemplates/saledirectiveview.html"
    }
})