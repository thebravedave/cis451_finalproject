-- MySQL dump 10.13  Distrib 5.7.18, for macos10.12 (x86_64)
--
-- Host: localhost    Database: cis451_finalproject
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `automobile`
--

DROP TABLE IF EXISTS `automobile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `automobile` (
  `vin` varchar(100) NOT NULL,
  `make` varchar(45) NOT NULL,
  `model` varchar(45) NOT NULL,
  `customerid` int(11) NOT NULL,
  PRIMARY KEY (`vin`),
  KEY `customerid_automobile_idx` (`customerid`),
  CONSTRAINT `customerid_automobile` FOREIGN KEY (`customerid`) REFERENCES `customer` (`customerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `automobile`
--

LOCK TABLES `automobile` WRITE;
/*!40000 ALTER TABLE `automobile` DISABLE KEYS */;
INSERT INTO `automobile` VALUES ('4H3LK213LK','Nissan','Sentra',1),('4KJLJ34LJK','Ford','Altima',3),('4KJLJKLJSDLK','Ford','Puller',4),('4KJLJKLKJLKJLKLK','Nissan','Sentra',5),('4KLJ34LJK','Ford','Taurus',2),('KJHKLJ234L','Ford','Ultima',8),('LKJ23LK2JL54','Honda','Fullback',10),('LKJ34L54','Honda','Hatchback',9),('LKJ34LK2J3','Nissan','BigRig',7),('LKL4KJ53L54','Honda','Mover',11),('OIUT4OP5','Nissan','MiniMover',6);
/*!40000 ALTER TABLE `automobile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companysales`
--

DROP TABLE IF EXISTS `companysales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companysales` (
  `orderid` int(11) NOT NULL,
  `ordertype` varchar(45) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`orderid`),
  KEY `ordertype_companysales_idx` (`ordertype`),
  CONSTRAINT `orderid_companysales` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ordertype_companysales` FOREIGN KEY (`ordertype`) REFERENCES `ordertype` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companysales`
--

LOCK TABLES `companysales` WRITE;
/*!40000 ALTER TABLE `companysales` DISABLE KEYS */;
INSERT INTO `companysales` VALUES (1,'sales order',4555.99),(2,'sales order',3955.99),(3,'sales order',9998.99),(4,'sales order',12298.99),(5,'sales order',7298.99),(6,'sales order',6669.99),(7,'work order',318),(8,'work order',1007),(9,'work order',957),(10,'work order',315),(11,'work order',2205);
/*!40000 ALTER TABLE `companysales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customerid` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `phone` varchar(45) NOT NULL,
  PRIMARY KEY (`customerid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'David','Thompson','123 Fake street','Alamar','California',92345,'5416541234'),(2,'Mark','Tilda','456 Daemon Street','Alamar','California',92345,'5434568765'),(3,'Pam','Mastra','567 Todd Street','Alamar','California',92345,'5434521365'),(4,'Carol','Zower','897 Todal Street','Alamar','California',92345,'5439991365'),(5,'Xander','Golmblast','976 Doodle Street','Alamar','California',92343,'5432221395'),(6,'Grace','Volgstra','456 Yvonne Street','Alamar','California',92343,'5435521665'),(7,'Sharon','Tree','421 Heckler Street','Alamar','California',92349,'5435121655'),(8,'Clyde','Alder','921 Bibbly Street','Grengar','California',92339,'5438771685'),(9,'Nick','Bruder','789 Myown Street','Grengar','California',92338,'5438341695'),(10,'Yvonne','Brower','745 True Street','Reerer','California',92348,'5438231698'),(11,'Tyee','Madoolie','735 Bobbidy Street','Smallsville','California',92347,'5438761645');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employeeid` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `socialsecurity` varchar(45) NOT NULL,
  PRIMARY KEY (`employeeid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Bob','Mechaner','542763589'),(2,'Bobby','Reger','542734568'),(3,'Tim','Reder','577765568'),(4,'Mitt','Rogleuy','578765988'),(5,'Jim','Zikowsky','578345988'),(6,'Jivon','Ytolle','578345988'),(7,'Dave','Nonne','578345988'),(8,'Wendy','Wenderson','519545988'),(9,'Jommer','Jammer','989543488'),(10,'Helber','Ironhammer','876543128'),(11,'Smoo','Thtalker','766512328'),(12,'Reber','Looler','712312328'),(13,'Jim','Remmer','912312312');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `orderid` int(11) NOT NULL,
  `hours` varchar(45) NOT NULL,
  PRIMARY KEY (`orderid`),
  UNIQUE KEY `orderid_UNIQUE` (`orderid`),
  CONSTRAINT `orderid_invoice` FOREIGN KEY (`orderid`) REFERENCES `workorder` (`orderid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (7,'6'),(8,'19'),(9,'29'),(10,'5'),(11,'35');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobtype`
--

DROP TABLE IF EXISTS `jobtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobtype` (
  `jobname` varchar(45) NOT NULL,
  `costperhour` double NOT NULL,
  PRIMARY KEY (`jobname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobtype`
--

LOCK TABLES `jobtype` WRITE;
/*!40000 ALTER TABLE `jobtype` DISABLE KEYS */;
INSERT INTO `jobtype` VALUES ('alignment',35),('carburetor',33),('electrical',35),('engine',63),('transmission',53);
/*!40000 ALTER TABLE `jobtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mechanic`
--

DROP TABLE IF EXISTS `mechanic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mechanic` (
  `employeeid` int(11) NOT NULL,
  PRIMARY KEY (`employeeid`),
  UNIQUE KEY `employeeid_UNIQUE` (`employeeid`),
  CONSTRAINT `employeeid_mechanic` FOREIGN KEY (`employeeid`) REFERENCES `employee` (`employeeid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mechanic`
--

LOCK TABLES `mechanic` WRITE;
/*!40000 ALTER TABLE `mechanic` DISABLE KEYS */;
INSERT INTO `mechanic` VALUES (1),(2),(4),(6),(8),(10),(12);
/*!40000 ALTER TABLE `mechanic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2018-11-08 06:50:02'),(2,'2018-11-08 06:50:13'),(3,'2018-11-08 06:50:19'),(4,'2018-11-08 06:50:29'),(5,'2018-11-08 06:50:32'),(6,'2018-11-08 06:50:45'),(7,'2018-11-08 06:50:48'),(8,'2018-11-08 06:50:52'),(9,'2018-11-08 06:50:55'),(10,'2018-11-08 06:50:59'),(11,'2018-11-08 06:51:02'),(12,'2018-11-08 06:51:06'),(13,'2018-11-08 06:51:08'),(14,'2018-11-08 06:51:13'),(15,'2018-11-08 06:51:30'),(16,'2018-11-08 06:51:33'),(17,'2018-11-08 06:51:36'),(18,'2018-11-08 06:51:38'),(19,'2018-11-08 06:51:42'),(20,'2018-11-08 06:51:46');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordertype`
--

DROP TABLE IF EXISTS `ordertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordertype` (
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordertype`
--

LOCK TABLES `ordertype` WRITE;
/*!40000 ALTER TABLE `ordertype` DISABLE KEYS */;
INSERT INTO `ordertype` VALUES ('sales order'),('work order');
/*!40000 ALTER TABLE `ordertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesman`
--

DROP TABLE IF EXISTS `salesman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesman` (
  `employeeid` int(11) NOT NULL,
  PRIMARY KEY (`employeeid`),
  UNIQUE KEY `employeeid_UNIQUE` (`employeeid`),
  CONSTRAINT `employeeid_salesman` FOREIGN KEY (`employeeid`) REFERENCES `employee` (`employeeid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesman`
--

LOCK TABLES `salesman` WRITE;
/*!40000 ALTER TABLE `salesman` DISABLE KEYS */;
INSERT INTO `salesman` VALUES (3),(5),(7),(9),(11),(13);
/*!40000 ALTER TABLE `salesman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesorder`
--

DROP TABLE IF EXISTS `salesorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesorder` (
  `orderid` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `vin` varchar(45) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`orderid`),
  UNIQUE KEY `vin_UNIQUE` (`vin`),
  UNIQUE KEY `orderid_UNIQUE` (`orderid`),
  KEY `employeeid_salesorder_idx` (`employeeid`),
  KEY `vin_salesorder_idx` (`vin`),
  CONSTRAINT `employeeid_salesorder` FOREIGN KEY (`employeeid`) REFERENCES `salesman` (`employeeid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `orderid_sales` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vin_salesorder` FOREIGN KEY (`vin`) REFERENCES `automobile` (`vin`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesorder`
--

LOCK TABLES `salesorder` WRITE;
/*!40000 ALTER TABLE `salesorder` DISABLE KEYS */;
INSERT INTO `salesorder` VALUES (1,3,'4H3LK213LK',4555.99),(2,5,'4KJLJ34LJK',3955.99),(3,7,'4KJLJKLJSDLK',9998.99),(4,9,'4KJLJKLKJLKJLKLK',12298.99),(5,11,'4KLJ34LJK',7298.99),(6,13,'KJHKLJ234L',6669.99);
/*!40000 ALTER TABLE `salesorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder`
--

DROP TABLE IF EXISTS `workorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder` (
  `orderid` int(11) NOT NULL,
  `vin` varchar(100) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `jobtype` varchar(45) NOT NULL,
  PRIMARY KEY (`orderid`),
  KEY `vin_workorder_idx` (`vin`),
  KEY `employeeid_workorder_idx` (`employeeid`),
  KEY `jobtype_workorder_idx` (`jobtype`),
  CONSTRAINT `employeeid_workorder` FOREIGN KEY (`employeeid`) REFERENCES `mechanic` (`employeeid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `jobtype_workorder` FOREIGN KEY (`jobtype`) REFERENCES `jobtype` (`jobname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `orderid_workorder` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vin_workorder` FOREIGN KEY (`vin`) REFERENCES `automobile` (`vin`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder`
--

LOCK TABLES `workorder` WRITE;
/*!40000 ALTER TABLE `workorder` DISABLE KEYS */;
INSERT INTO `workorder` VALUES (7,'LKJ23LK2JL54',1,'transmission'),(8,'LKJ34L54',2,'transmission'),(9,'LKJ34LK2J3',4,'carburetor'),(10,'LKJ34LK2J3',6,'engine'),(11,'OIUT4OP5',8,'engine');
/*!40000 ALTER TABLE `workorder` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-09 19:33:15
