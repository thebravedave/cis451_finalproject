<?php
/***************************************************************************************
 ****************************************************************************************
 *     All code shown copyright of Binary Web Design LLC. Copyright 2013. ***************
 ****************************************************************************************
 ***************************************************************************************/



class DatabaseConfigurationInformation{
    public $server;
    public $database;
    public $username;
    public $password;
    public $port;
}

class Success{
    public $success = true;
    public $message;
}

class Employee{
    public $employeeId;
    public $firstName;
    public $lastName;
    public $socialSecurity;
}

class Mechanic extends Employee {}

class Salesman extends Employee {}

class Automobile{
    public $vin;
    public $make;
    public $model;
    public $customerId;
}

class CompanySale{
    public $order;
    public $orderType;
    public $amount;
}

class Customer{
    public $customerId;
    public $firstName;
    public $lastName;
    public $address;
    public $city;
    public $state;
    public $zipcode;
    public $phone;
}

class Invoice{
    public $orderId;
    public $hours;
}

class JobType{
    public $jobName;
    public $costPerHour;
}

class Orders{
    public $orderId;
    public $dateTime;
}

class OrderType{
    public $name;
}

class Order{
    public $orderId;
    public $date;
}

class SalesOrder extends Order{
    public $salesman;
    public $automobile;
    public $customer;
    public $amount;
}


class WorkOrder extends Order{
    public $charged;
    public $automobile;
    public $mechanic;
    public $customer;
    public $jobType;
    public $invoice;
}

class MechanicWorkOrders{
    public $mechanic;
    public $workOrders;
}

class SalesManSalesOrders{
    public $salesman;
    public $salesOrders;
}
























?>
