<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');



session_start();

include_once("../dto/datatransferobjects.php");
include_once("../managers/storemanager.php");
include_once("../dal/storedao.php");

include_once("../util/databaseutil.php");
include_once("../util/generalutil.php");

require '../lib/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'debug' => true
));



$app->get('/customers/', function() use($app) {

    $storeManager = new StoreManager();


    $customers = $storeManager->getAllCustomers();

    $success = new Success();
    $success->message = $customers;
    $json = json_encode($success);
    echo $json;

}); //customers/'

$app->get('/sales', function() use($app) {

    $storeManager = new StoreManager();
    $salesmen = $storeManager->getSales();
    $success = new Success();
    $success->message = $salesmen;
    $json = json_encode($success);
    echo $json;

}); //sales'

$app->get('/sale/:orderId/:saleType', function($orderId, $orderType) use($app) {

    $storeManager = new StoreManager();
    $salesmen = $storeManager->getSale($orderId, $orderType);
    $success = new Success();
    $success->message = $salesmen;
    $json = json_encode($success);
    echo $json;

}); //sales'

$app->get('/mechanics', function() use($app) {

    $storeManager = new StoreManager();


    $mechanics = $storeManager->getMechanics();

    $success = new Success();
    $success->message = $mechanics;
    $json = json_encode($success);
    echo $json;

}); //mechanics'

$app->get('/salesmen', function() use($app) {

    $storeManager = new StoreManager();


    $salesmen = $storeManager->getSalesmen();

    $success = new Success();
    $success->message = $salesmen;
    $json = json_encode($success);
    echo $json;

}); //salesmen'

$app->get('/mechanic/workorders/:employeeId', function($employeeId) use($app) {

    $storeManager = new StoreManager();


    $workOrders = $storeManager->getWorkOrdersForMechanic($employeeId);

    $success = new Success();
    $success->message = $workOrders;
    $json = json_encode($success);
    echo $json;

}); //mechanic/workorders/:employeeId'

$app->get('/workorder/:orderId', function($orderId) use($app) {

    $storeManager = new StoreManager();


    $workOrders = $storeManager->getWorkOrdersForMechanic($orderId);

    $success = new Success();
    $success->message = $workOrders;
    $json = json_encode($success);
    echo $json;

}); //workorder/:orderId'


$app->get('/salesman/salesorders/:employeeId', function($employeeId) use($app) {

    $storeManager = new StoreManager();


    $workOrders = $storeManager->getSalesOrdersForSalesman($employeeId);

    $success = new Success();
    $success->message = $workOrders;
    $json = json_encode($success);
    echo $json;

}); //salesman/salesorders/:employeeId'


$app->get('/salesorder/:orderId', function($orderId) use($app) {

    $storeManager = new StoreManager();


    $salesOrders = $storeManager->getSalesOrderByOrderId($orderId);

    $success = new Success();
    $success->message = $salesOrders;
    $json = json_encode($success);
    echo $json;

}); //salesorder/:$orderId'


$app->run();

