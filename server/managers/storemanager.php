<?php

class StoreManager{
    public $baseUrl = "http://api.wunderground.com/api/";
    function __construct(){}
    public function getAllCustomers(){
        $customersDAO = new StoreDAO();
        $customers = $customersDAO->getAllCustomers();
        return $customers;
    }
    public function getCustomerInformationByCustomerId($customerId){
        $customersDAO = new StoreDAO();
        $customers = $customersDAO->getCustomerInformationById($customerId);
        return $customers;
    }

    public function getMechanics(){
        $storeDAO = new StoreDAO();
        return $storeDAO->getAllMechanics();
    }
    public function getSalesmen(){
        $storeDAO = new StoreDAO();
        return $storeDAO->getAllSalesmen();
    }

    public function getWorkOrdersForMechanic($employeeId)
    {
        $storeDAO = new StoreDAO();
        return $storeDAO->getWorkOrdersForMechanic($employeeId);

    }

    public function getSales()
    {
        $storeDAO = new StoreDAO();
        return $storeDAO->getSales();

    }

    public function getWorkOrderByOrderId($orderId)
    {
        $storeDAO = new StoreDAO();
        return $storeDAO->getWorkOrdersForMechanic($orderId);

    }


    public function getSalesOrdersForSalesman($employeeId)
    {
        $storesDAO = new StoreDAO();
        return $storesDAO->getSalesOrdersForSalesman($employeeId);
    }

    public function getSalesOrderByOrderId($orderId)
    {
        $storesDAO = new StoreDAO();
        return $storesDAO->getSalesOrderByOrderId($orderId);
    }

    public function getSale($orderId, $orderType)
    {
        $storesDAO = new StoreDAO();
        return $storesDAO->getSaleByOrderIdAndOrderType($orderId, $orderType);
    }
}

























