<?php
/**
 * Created by IntelliJ IDEA.
 * User: david
 * Date: 11/1/18
 * Time: 3:55 PM
 */

class StoreDAO
{
    private $mysqli;

    function __construct()
    {
        $databaseInformation = DatabaseUtil::getDatabaseConfigurationDetails();
        if ($databaseInformation == null)
            return;

        $this->site_dbServer = $databaseInformation->server;
        $this->site_dbName = $databaseInformation->database;
        $this->site_dbUser = $databaseInformation->username;
        $this->site_dbPassword = $databaseInformation->password;
        $this->port = $databaseInformation->port;
        $port = (int) $this->port;
        $mysqli = new mysqli($this->site_dbServer, $this->site_dbUser, $this->site_dbPassword, $this->site_dbName, $port);
        $this->mysqli = $mysqli;
    }


    public function getAllMechanics(){
        $mechanics = array();
        $query = "SELECT employee.* FROM mechanic
                  JOIN employee USING(employeeid)
                  ORDER BY lastname ASC";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->execute();
            $stmt->bind_result($employeeId, $firstName, $lastName, $socialSecurity);
            $stmt->store_result();
            while ($stmt->fetch())
            {

                $mechanic = new Mechanic();
                $mechanic->employeeId = $employeeId;
                $mechanic->firstName = $firstName;
                $mechanic->lastName = $lastName;
                $mechanic->socialSecurity = $socialSecurity;

                $mechanics[sizeof($mechanics)] = $mechanic;

            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $mechanics;
    }

    public function getAllSalesmen(){
        $salesmen = array();
        $query = "SELECT employee.* FROM salesman
                  JOIN employee USING(employeeid)
                  ORDER BY lastname ASC";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->execute();
            $stmt->bind_result($employeeId, $firstName, $lastName, $socialSecurity);
            $stmt->store_result();
            while ($stmt->fetch())
            {

                $salesman = new Salesman();
                $salesman->employeeId = $employeeId;
                $salesman->firstName = $firstName;
                $salesman->lastName = $lastName;
                $salesman->socialSecurity = $socialSecurity;

                $salesmen[sizeof($salesmen)] = $salesman;

            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $salesmen;
    }


    public function getSales(){

        $companySales = array();
        $query = "
                    SELECT * FROM companysales
                    JOIN orders USING(orderid)
                    ORDER BY orders.date ASC                                
                  ";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->execute();
            $stmt->bind_result($orderId, $orderType, $amount, $date);
            $stmt->store_result();
            while ($stmt->fetch())
            {

                $order = new Order();
                $order->orderId = $orderId;
                $order->date = $date;

                $companySale = new CompanySale();
                $companySale->order = $order;
                $companySale->orderType = $orderType;
                $companySale->amount = $amount;
                $companySales[sizeof($companySales)] = $companySale;

            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $companySales;
    }




    public function getWorkOrdersForMechanic($employeeId)
    {

        $mechanicWorkOrders = new MechanicWorkOrders();
        $workOrders = array();
        $query = "
        SELECT employee.*, customer.*, orders.date, workorder.*, automobile.*, invoice.*, jobtype.* FROM workorder
        JOIN jobtype ON jobtype.jobname = workorder.jobtype
        JOIN orders USING(orderid)
        JOIN invoice USING(orderid)
        JOIN automobile USING(vin)
        JOIN customer USING(customerid)
        RIGHT JOIN employee USING(employeeid)
        WHERE employee.employeeid = ?

           ";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->bind_param('i', $employeeId);
            $stmt->execute();
            $stmt->bind_result( $employeeId, $mechanicFirstName, $mechanicLastName, $mechanicSocialSecurity, $customerId, $customerFirstName, $customerLastname, $customerAddress, $customerCity, $customerState, $customerZipCode, $customerPhone, $orderDate, $orderId, $orderVin, $orderEmployeeId, $orderJobType, $vin, $make, $model, $autoCustId, $invoiceOrderId, $hours, $jobName,  $costPerHour);
            $stmt->store_result();

            while ($stmt->fetch())
            {
                $mechanic = new Mechanic();
                $mechanic->employeeId = $employeeId;
                $mechanic->firstName = $mechanicFirstName;
                $mechanic->lastName = $mechanicLastName;
                $mechanic->socialSecurity = $mechanicSocialSecurity;
                $mechanicWorkOrders->mechanic = $mechanic;
                if($orderId == null){
                    continue;
                }



                $automobile = new Automobile();
                $automobile->vin = $vin;
                $automobile->make = $make;
                $automobile->model = $model;
                $automobile->customerId = $customerId;

                $customer = new Customer();
                $customer->customerId = $customerId;
                $customer->firstName = $customerFirstName;
                $customer->lastName = $customerLastname;
                $customer->address = $customerAddress;
                $customer->city = $customerCity;
                $customer->state = $customerState;
                $customer->zipcode = $customerZipCode;
                $customer->phone = $customerPhone;

                $invoice = new Invoice();
                $invoice->orderId = $orderId;
                $invoice->hours = $hours;

                $jobType = new JobType();
                $jobType->jobName = $jobName;
                $jobType->costPerHour = $costPerHour;

                $workOrder = new WorkOrder();
                $workOrder->orderId = $orderId;
                $cost = (double) $costPerHour * (double) $hours;
                $cost = GeneralUtil::formatCurrency($cost);
                $workOrder->charged = $cost;
                $workOrder->date = $orderDate;
                $workOrder->automobile = $automobile;
                $workOrder->mechanic = $mechanic;
                $workOrder->customer = $customer;
                $workOrder->jobType = $jobType;
                $workOrder->invoice = $invoice;



                $workOrders[count($workOrders)] = $workOrder;

            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }
        $mechanicWorkOrders->workOrders = $workOrders;
        return $mechanicWorkOrders;


    }

    public function getWorkOrdersByOrderId($orderId)
    {
        $mechanicWorkOrders = new MechanicWorkOrders();
        $workOrders = array();
        $query = "
        SELECT employee.*, customer.*, orders.date, workorder.*, automobile.*, invoice.*, jobtype.* FROM workorder
        JOIN jobtype ON jobtype.jobname = workorder.jobtype
        JOIN orders USING(orderid)
        JOIN invoice USING(orderid)
        JOIN automobile USING(vin)
        JOIN customer USING(customerid)
        RIGHT JOIN employee USING(employeeid)
        WHERE orders.orderid = ?

           ";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->bind_param('i', $orderId);
            $stmt->execute();
            $stmt->bind_result( $employeeId, $mechanicFirstName, $mechanicLastName, $mechanicSocialSecurity, $customerId, $customerFirstName, $customerLastname, $customerAddress, $customerCity, $customerState, $customerZipCode, $customerPhone, $orderDate, $orderId, $orderVin, $orderEmployeeId, $orderJobType, $vin, $make, $model, $autoCustId, $invoiceOrderId, $hours, $jobName,  $costPerHour);
            $stmt->store_result();

            while ($stmt->fetch())
            {
                $mechanic = new Mechanic();
                $mechanic->employeeId = $employeeId;
                $mechanic->firstName = $mechanicFirstName;
                $mechanic->lastName = $mechanicLastName;
                $mechanic->socialSecurity = $mechanicSocialSecurity;
                $mechanicWorkOrders->mechanic = $mechanic;
                if($orderId == null){
                    continue;
                }

                $automobile = new Automobile();
                $automobile->vin = $vin;
                $automobile->make = $make;
                $automobile->model = $model;
                $automobile->customerId = $customerId;

                $customer = new Customer();
                $customer->customerId = $customerId;
                $customer->firstName = $customerFirstName;
                $customer->lastName = $customerLastname;
                $customer->address = $customerAddress;
                $customer->city = $customerCity;
                $customer->state = $customerState;
                $customer->zipcode = $customerZipCode;
                $customer->phone = $customerPhone;

                $invoice = new Invoice();
                $invoice->orderId = $orderId;
                $invoice->hours = $hours;

                $jobType = new JobType();
                $jobType->jobName = $jobName;
                $jobType->costPerHour = $costPerHour;

                $workOrder = new WorkOrder();
                $workOrder->orderId = $orderId;
                $cost = (double) $costPerHour * (double) $hours;
                $cost = GeneralUtil::formatCurrency($cost);
                $workOrder->charged = $cost;
                $workOrder->date = $orderDate;
                $workOrder->automobile = $automobile;
                $workOrder->mechanic = $mechanic;
                $workOrder->customer = $customer;
                $workOrder->jobType = $jobType;
                $workOrder->invoice = $invoice;



                $workOrders[count($workOrders)] = $workOrder;

            }
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }
        $mechanicWorkOrders->workOrders = $workOrders;
        return $mechanicWorkOrders;


    }

    public function getSalesOrdersForSalesman($employeeId)
    {
        $salesManSalesOrders = new SalesManSalesOrders();


        $salesOrders = array();
        $query = "
        SELECT * FROM salesorder
        JOIN orders USING(orderid)
        JOIN automobile USING(vin)
        JOIN customer USING(customerid)
        JOIN employee USING(employeeid)
        WHERE employee.employeeid = ?

           ";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->bind_param('i', $employeeId);
            $stmt->execute();
            $stmt->bind_result( $employeeId, $customerId, $vin, $orderId, $amount, $date, $make, $model, $customerFirstName, $customerLastname, $customerAddress, $customerCity, $customerState, $customerZipCode, $customerPhone, $salemanFirstName, $salesmanLastName, $salesmanSocialSecurity);
            $stmt->store_result();

            while ($stmt->fetch())
            {

                $salesman = new Salesman();
                $salesman->employeeId = $employeeId;
                $salesman->firstName = $salemanFirstName;
                $salesman->lastName = $salesmanLastName;
                $salesman->socialSecurity = $salesmanSocialSecurity;

                $salesManSalesOrders->salesman = $salesman;

                $automobile = new Automobile();
                $automobile->vin = $vin;
                $automobile->make = $make;
                $automobile->model = $model;
                $automobile->customerId = $customerId;

                $customer = new Customer();
                $customer->customerId = $customerId;
                $customer->firstName = $customerFirstName;
                $customer->lastName = $customerLastname;
                $customer->address = $customerAddress;
                $customer->city = $customerCity;
                $customer->state = $customerState;
                $customer->zipcode = $customerZipCode;
                $customer->phone = $customerPhone;


                $salesOrder = new SalesOrder();
                $salesOrder->orderId = $orderId;
                $salesOrder->salesman = $salesman;
                $salesOrder->customer = $customer;
                $salesOrder->date = $date;
                $salesOrder->automobile = $automobile;
                $salesOrder->amount = $amount;

                $salesOrders[count($salesOrders)] = $salesOrder;

            }
            $salesManSalesOrders->salesOrders = $salesOrders;
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $salesManSalesOrders;


    }

    public function getSalesOrderByOrderId($orderId)
    {
        $salesManSalesOrders = new SalesManSalesOrders();


        $salesOrders = array();
        $query = "
        SELECT * FROM salesorder
        JOIN orders USING(orderid)
        JOIN automobile USING(vin)
        JOIN customer USING(customerid)
        JOIN employee USING(employeeid)
        WHERE orders.orderid = ?

           ";
        $stmt = $this->mysqli->prepare($query);
        if($stmt) {
            $stmt->bind_param('i', $orderId);
            $stmt->execute();
            $stmt->bind_result( $employeeId, $customerId, $vin, $orderId, $amount, $date, $make, $model, $customerFirstName, $customerLastname, $customerAddress, $customerCity, $customerState, $customerZipCode, $customerPhone, $salemanFirstName, $salesmanLastName, $salesmanSocialSecurity);
            $stmt->store_result();

            while ($stmt->fetch())
            {

                $salesman = new Salesman();
                $salesman->employeeId = $employeeId;
                $salesman->firstName = $salemanFirstName;
                $salesman->lastName = $salesmanLastName;
                $salesman->socialSecurity = $salesmanSocialSecurity;

                $salesManSalesOrders->salesman = $salesman;

                $automobile = new Automobile();
                $automobile->vin = $vin;
                $automobile->make = $make;
                $automobile->model = $model;
                $automobile->customerId = $customerId;

                $customer = new Customer();
                $customer->customerId = $customerId;
                $customer->firstName = $customerFirstName;
                $customer->lastName = $customerLastname;
                $customer->address = $customerAddress;
                $customer->city = $customerCity;
                $customer->state = $customerState;
                $customer->zipcode = $customerZipCode;
                $customer->phone = $customerPhone;


                $salesOrder = new SalesOrder();
                $salesOrder->orderId = $orderId;
                $salesOrder->salesman = $salesman;
                $salesOrder->customer = $customer;
                $salesOrder->date = $date;
                $salesOrder->automobile = $automobile;
                $salesOrder->amount = $amount;

                $salesOrders[count($salesOrders)] = $salesOrder;

            }
            $salesManSalesOrders->salesOrders = $salesOrders;
            $stmt->free_result();
            $stmt->close();
        }
        else{
            var_dump($this->mysqli->error);
        }

        return $salesManSalesOrders;


    }

    public function getSaleByOrderIdAndOrderType($orderId, $orderType)
    {
        $companySale = new CompanySale();
        $companySale->orderType = $orderType;

        if($orderType == "sales order"){
            return $this->getSalesOrderByOrderId($orderId);

        }
        else if($orderType == "work order"){
            return $this->getWorkOrdersByOrderId($orderId);

        }

    }

}